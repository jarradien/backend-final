package fr.formation.correction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Personne;
import fr.formation.correction.model.dto.FiliereDTO;
import fr.formation.correction.service.FiliereService;

@CrossOrigin
@RestController
@RequestMapping("/api/filiere")
public class FiliereController {


    @Autowired
    FiliereService filiereService;

    @GetMapping("")
    public List<Filiere> findAll() {
        return filiereService.findAll();
    }

    @GetMapping("/{id}")
    public Filiere getById(@PathVariable Integer id) {
        return filiereService.getById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La filière n'existe pas"));
    }

    /*
     * Il est possible d'affecter des stagiaires a la filière créer directement En
     * mettant une liste de stagiaire (leur id) dans le body de la requète (détail
     * dans la méthode create de FiliereService)
     */
    @PostMapping("")
    public void createFiliere(@RequestBody Filiere f) {
        filiereService.create(f);
    }

    @PutMapping("")
    public void updateFiliere(@RequestBody Filiere f) {
        filiereService.update(f);
    }

    @DeleteMapping("/{id}")
    public void deleteFiliere(@PathVariable Integer id) {
        filiereService.delete(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La filière n'existe pas"));
    }

    /*
     * Renvoie la filière avec sa date de début et fin selon les modules associés
     */
    @GetMapping("/detail/{id}")
    public FiliereDTO getByIdWithDetails(@PathVariable Integer id) {
        return filiereService.getByIdWithDetail(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La filière n'existe pas"));
    }

    /*
     * Affecte la liste de personnes donnée en body de la requète à la filière mis dans l'url
     */
    @PutMapping("/stagiaires/{id}")
    public void setStagiairesOfFiliere(@RequestBody List<Personne> personnes, @PathVariable Integer id) {
        filiereService.setStagiairesOfFiliere(personnes, id);
    }

    @GetMapping("/libelle/{libelle}")
    public List<Filiere> getFiliereByLibelle(@PathVariable String libelle) {
        return filiereService.getFiliereByLibelle(libelle);
    }

}
