package fr.formation.correction.controller;

import fr.formation.correction.model.Formation;
import fr.formation.correction.service.FormationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/formation")
public class FormationController {

    @Autowired
    FormationService formationService;

    @GetMapping("")
    public List<Formation> findAll() {
        return formationService.findAll();
    }

    @GetMapping("/{id}")
    public Formation getById(@PathVariable String id) {
        return formationService.getById(id).orElse(null);
    }

    @PostMapping("")
    public void createFormation(@RequestBody Formation f) {
        formationService.create(f);
    }

    @PutMapping("")
    public void updateFormation(@RequestBody Formation f) {
        formationService.update(f);
    }

    @DeleteMapping("/{id}")
    public void deleteFormation(@PathVariable String id) {
        formationService.delete(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Le Formation n'existe pas"));
    }


}
