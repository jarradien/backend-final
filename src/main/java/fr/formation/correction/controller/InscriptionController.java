package fr.formation.correction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.correction.model.User;
import fr.formation.correction.service.UserService;

@RestController
@RequestMapping("/inscription")
public class InscriptionController {
	
	@Autowired
	UserService repo;
	
	@PostMapping("")
	public String inscription(@RequestBody User u) {
		repo.inscription(u);
		return "Hello World";
	}

}
