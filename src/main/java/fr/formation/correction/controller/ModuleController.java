package fr.formation.correction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.correction.model.Module;
import fr.formation.correction.service.ModuleService;

@CrossOrigin
@RestController
@RequestMapping("/api/module")
public class ModuleController {

    @Autowired
    private ModuleService moduleService;


    @GetMapping("")
    public List<Module> findAll() {
        return moduleService.findAll();
    }

    @GetMapping("/{id}")
    public Module getById(@PathVariable Integer id) {
        return moduleService.getById(id).orElse(null);
    }

    @PostMapping("")
    public void createModule(@RequestBody Module p) {
        moduleService.create(p);
    }

    @PutMapping("")
    public void updateModule(@RequestBody Module p) {
        moduleService.update(p);
    }

    @DeleteMapping("/{id}")
    public void deleteModule(@PathVariable Integer id) {
        moduleService.delete(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Le module n'existe pas"));
    }

    /*
     * Renvoie les modules d'une filière
     */
    @GetMapping("/filiere/{id}")
    public List<Module> getModulesOfFiliere(@PathVariable Integer id) {
        return moduleService.findByFiliereId(id);
    }

    /*
     * Renvoie les modules qui n'ont pas de formateur assignés
     */
    @GetMapping("/formateurMissing")
    public List<Module> getModuleWithoutFormateur() {
        return moduleService.getModuleWithoutFormateur();
    }

    @GetMapping("/formateur/{formateur}")
    public List<Module> getModuleByFormateur(@PathVariable String formateur) {
        System.out.println(" formateur : " + formateur);
        return moduleService.getModulesByFormateur(formateur);
    }

}
