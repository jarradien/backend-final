package fr.formation.correction.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Personne;
import fr.formation.correction.service.PersonneService;

@CrossOrigin
@RestController
@RequestMapping("/api/personne")
public class PersonneController {

    @Autowired
    private PersonneService personneService;

    @GetMapping("")
    public List<Personne> findAll() {
        return personneService.findAll();
    }

    @GetMapping("/{id}")
    public Personne getById(@PathVariable Integer id) {
        return personneService.getById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La personne n'existe pas"));
    }

    @PostMapping("")
    public void createPersonne(@RequestBody Personne p) {
        personneService.create(p);
    }

    @PutMapping("")
    public void updatePersonne(@RequestBody Personne p) {
        personneService.update(p);
    }

    @DeleteMapping("/{id}")
    public void deletePersonne(@PathVariable Integer id) {
        personneService.delete(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "La personne n'existe pas"));
    }

    /*
     * Renvoie la liste des formateurs intervenant sur une filière donné en
     * paramètre
     */
    @GetMapping("/formateurs")
    public List<Personne> getFormateursOfFiliere(@RequestParam(name = "filiere_id") Integer id) {
        Filiere f = new Filiere();
        f.setId(id);
        return personneService.getFormateursOfFiliere(f);
    }

    @GetMapping("/identification")
    public Personne identification(@RequestParam(name = "login") String login,
                                   @RequestParam(name = "password") String password) {

        Personne p = personneService.getWithLogin(login);

        if (p == null || !p.getMotDePasse().equals(password)) {
            return null;
        }
        return p;
    }

    @GetMapping("/souhait/{formationSouhaitee}")
    public List<Personne> getPersonneParSouhait(@PathVariable String formationSouhaitee) {
        return personneService.getWithFormationSouhaitee(formationSouhaitee);
    }

    @GetMapping("/formateurs/all")
    public List<Personne> getAllFormateurs() {
        return personneService.getAllFormateurs();
    }

    @GetMapping("/nouveaux")
    public List<Personne> getAllNouveaux() {
        return personneService.getAllNouveaux();
    }

}
