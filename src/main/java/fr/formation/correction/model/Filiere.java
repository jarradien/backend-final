package fr.formation.correction.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import jakarta.persistence.*;
import java.util.List;

@Data
@Entity
@SequenceGenerator(name = "filiere_gen", sequenceName = "filiere_seq", allocationSize = 1)
public class Filiere {

    @Id
    @GeneratedValue(generator = "filiere_gen")
    private Integer id;

    private String libelle;

    @OneToMany(mappedBy = "filiere")
    @JsonIgnoreProperties("filiere")
    private List<Module> modules;

    @OneToMany(mappedBy = "filiere")
    @JsonIgnoreProperties("filiere")
    private List<Personne> stagiaires;

    @ManyToOne
    @JoinColumn(name = "formation_id")
    @JsonIgnoreProperties("filieres")
    private Formation formation;

}
