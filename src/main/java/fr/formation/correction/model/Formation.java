package fr.formation.correction.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.util.List;

@Data
@Entity
public class Formation {

	@Id
	private String libelle;

	@OneToMany(mappedBy = "formation")
	@JsonIgnoreProperties("formation")
	private List<Filiere> filieres;

}
