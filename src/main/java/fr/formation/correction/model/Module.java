package fr.formation.correction.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import jakarta.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class Module {

    @Id
    @GeneratedValue
    private Integer id;

    private String libelle;

    private LocalDate dateDebut;

    private LocalDate dateFin;

    @ManyToOne
    @JoinColumn(name = "personne_id")
    @JsonIgnoreProperties("modules")
    private Personne formateur;

    @ManyToOne
    @JoinColumn(name = "filiere_id")
    @JsonIgnoreProperties("modules")
    private Filiere filiere;

}
