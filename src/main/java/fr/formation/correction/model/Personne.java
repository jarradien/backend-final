package fr.formation.correction.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import jakarta.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@SequenceGenerator(name = "personne_gen", sequenceName = "personne_seq", initialValue = 100, allocationSize = 1)
public class Personne {

    @Id
    @GeneratedValue(generator = "personne_gen")
    private Integer id;

    private String nom;
    private String prenom;
    private String email;
    private String telephone;
    private LocalDate dateNaissance;
    private String adressePostale;
    private String formationSouhaitee;
    private String login;
    private String motDePasse;

    @Enumerated(EnumType.STRING)
    private TypePersonne type;

    @ManyToOne
    @JoinColumn(name = "filiere_id")
    @JsonIgnoreProperties("stagiaires")
    private Filiere filiere;

    @OneToMany(mappedBy = "formateur")
    @JsonIgnoreProperties("formateur")
    private List<Module> modules;

}
