package fr.formation.correction.model;

public enum TypePersonne {
    NOUVEAU("nouveau"), STAGIAIRE("stagiaire"), FORMATEUR("formateur"), GESTIONNAIRE("gestionnaire");

    private String libelle;

    TypePersonne(String libelle) {
        this.libelle = libelle;
    }

    public String toString() {
        return this.libelle;
    }

    public static TypePersonne fromString(String libelle) {
        for (TypePersonne tp : values()) {
            if (tp.libelle.equals(libelle))
                return tp;
        }
        return null;
    }

}
