package fr.formation.correction.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity(name = "user_table")
public class User {

	@Id
	private String username;

	private String password;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	@JsonIgnoreProperties("user")
	private List<UserRole> roles;

//	@JsonIgnore
//	public Collection<? extends GrantedAuthority> getRolesAsAuthorities() {
//		return this.roles.stream().map(r -> new SimpleGrantedAuthority(r.getRole().name()))
//				.collect(Collectors.toList());
//	}

	public UserDetails toUserDetails() {
		List<GrantedAuthority> authorities = new ArrayList<>();
		
		for(UserRole role : roles) {
			GrantedAuthority ga = new SimpleGrantedAuthority(role.getRole().name());
			authorities.add(ga);
		}		
		
		return new org.springframework.security.core.userdetails.User(this.username, this.password,authorities);
	}

}
