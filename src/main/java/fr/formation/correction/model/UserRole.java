package fr.formation.correction.model;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

@Data
@Entity
@SequenceGenerator(name = "user_role_gen", sequenceName = "user_role_seq", initialValue = 100, allocationSize = 1)
public class UserRole {
    @Id
    @GeneratedValue(generator = "user_role_gen")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_username")
    @JsonIgnoreProperties("roles")
    private User user;

    @Enumerated(EnumType.STRING)
    private Role role;

    public UserRole(User user, Role role) {
        this.user = user;
        this.role = role;
    }

}
