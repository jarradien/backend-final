package fr.formation.correction.model.dto;

import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Module;
import fr.formation.correction.model.Personne;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class FiliereDTO {

    private Integer id;

    private String libelle;

    private List<Module> modules;

    private List<Personne> stagiaires;

    private LocalDate dateDebut;
    private LocalDate dateFin;

    public FiliereDTO(Integer id, String libelle, List<Module> modules, List<Personne> stagiaires) {
        super();
        this.id = id;
        this.libelle = libelle;
        this.modules = modules;
        this.stagiaires = stagiaires;
    }

    public static FiliereDTO fromFiliere(Filiere f) {
        return new FiliereDTO(f.getId(), f.getLibelle(), f.getModules(), f.getStagiaires());
    }

}
