package fr.formation.correction.repository;

import fr.formation.correction.model.Filiere;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FiliereRepository extends JpaRepository<Filiere, Integer> {
	
	List<Filiere> findByLibelle(String libelle);

}
