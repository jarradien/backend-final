package fr.formation.correction.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.correction.model.Formation;

public interface FormationRepository extends JpaRepository<Formation, String> {

}
