package fr.formation.correction.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Module;
import fr.formation.correction.model.Personne;

public interface ModuleRepository extends JpaRepository<Module, Integer> {

	List<Module> findByFiliere(Filiere f);
	// TODO : implement findByFormateurName or FirstAndLastName
//	List<Module> findByFormateur(String formateur);

	List<Module> findByFormateurIsNull();
	
//	@Query("SELECT m FROM Module m JOIN m.modules m WHERE p.type = 'FORMATEUR' AND m.filiere = :f")
//	List<Module> findModulesByFormateur(@Param("f") Filiere f);
}
