package fr.formation.correction.repository;

import java.util.List;
import java.util.Optional;

import jakarta.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Personne;

public interface PersonneRepository extends JpaRepository<Personne, Integer> {
	@Modifying
	@Query("UPDATE Personne p SET p.filiere = :f WHERE p.id=:pId AND p.type='STAGIAIRE'")
	@Transactional
	void bindPersonneToFiliere(@Param("pId") Integer personneId, @Param("f") Filiere filiere);

	@Query("SELECT p FROM Personne p JOIN p.modules m WHERE p.type = 'FORMATEUR' AND m.filiere = :f")
	List<Personne> findFormateursOfFiliere(@Param("f") Filiere f);
	
	@Query("SELECT p FROM Personne p WHERE p.type = 'FORMATEUR'")
	List<Personne> findAllFormateur();
	
	@Query("SELECT p FROM Personne p WHERE p.type = 'Nouveau'")
	List<Personne> findAllNouveaux();
	
	@Query("SELECT p.login FROM Personne p")
	List<String> listAllLogin();
	
	Optional<Personne> findByLogin(String login);
	List<Personne> findByFormationSouhaitee( String fs);
}
