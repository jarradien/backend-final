package fr.formation.correction.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.formation.correction.model.User;

public interface UserRepository extends JpaRepository<User,Integer>{
	Optional<User> findByUsername(String name);
}
