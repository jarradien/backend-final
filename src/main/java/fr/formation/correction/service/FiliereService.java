package fr.formation.correction.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.correction.controller.exception.BindStagiaireToFiliereException;
import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Module;
import fr.formation.correction.model.Personne;
import fr.formation.correction.model.dto.FiliereDTO;
import fr.formation.correction.repository.FiliereRepository;

@Service
public class FiliereService {

    @Autowired
    private FiliereRepository filiereRepository;

    @Autowired
    private PersonneService personneService;

	public void create(Filiere f) {
		// TODO handle exception when there are not only interns ?
		if (personneService.checkAllStagiaire(f.getStagiaires())) {
			Filiere persistedFiliere = this.filiereRepository.save(f);
			for (Personne p : f.getStagiaires()) {
				personneService.bindStagiaireToFiliere(p, persistedFiliere);
			}
		}
	}

    public List<Filiere> findAll() {
        return this.filiereRepository.findAll();
    }

    public Optional<Filiere> getById(Integer id) {
        return this.filiereRepository.findById(id);
    }

    public void update(Filiere f) {
        this.filiereRepository.save(f);
    }

    public Optional<Boolean> delete(Filiere f) {
        try {
            this.filiereRepository.delete(f);
            return Optional.of(Boolean.TRUE);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public Optional<Boolean> delete(Integer id) {
        try {
            this.filiereRepository.deleteById(id);
            return Optional.of(Boolean.TRUE);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public Optional<FiliereDTO> getByIdWithDetail(Integer id) {
        Optional<Filiere> foundFiliere = filiereRepository.findById(id);
        if (foundFiliere.isEmpty()) {

			return Optional.empty();
		}
        FiliereDTO dto = FiliereDTO.fromFiliere(foundFiliere.get());
        LocalDate minDate = null;
        LocalDate maxDate = null;
        for (Module m : dto.getModules()) {
            if (minDate == null || minDate.isAfter(m.getDateDebut())) minDate = m.getDateDebut();
            if (maxDate == null || maxDate.isBefore(m.getDateFin())) maxDate = m.getDateFin();
        }
        dto.setDateDebut(minDate);
        dto.setDateFin(maxDate);

        return Optional.of(dto);
    }

    public void setStagiairesOfFiliere(List<Personne> pList, Integer filiereId) {
        Filiere f = new Filiere();
        f.setId(filiereId);
        if (!personneService.checkAllStagiaire(pList)) throw new BindStagiaireToFiliereException();
        personneService.bindStagiairesToFiliere(pList, f);
    }

    public List<Filiere> getFiliereByLibelle(String libelle) {
        return filiereRepository.findByLibelle(libelle);
    }
}
