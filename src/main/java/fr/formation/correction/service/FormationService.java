package fr.formation.correction.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.correction.model.Formation;
import fr.formation.correction.repository.FormationRepository;

@Service
public class FormationService {

	@Autowired
	private FormationRepository formationRepository;
	
	public void create(Formation f) {
		this.formationRepository.save(f);
	}
	
	public List<Formation> findAll() {
		return this.formationRepository.findAll();
	}
	
	public Optional<Formation> getById(String id) {
		return this.formationRepository.findById(id);
	}
	
	public void update(Formation f) {
		this.formationRepository.save(f);
	}
	
	public Optional<Boolean> delete(Formation f) {
		try {
			this.formationRepository.delete(f);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
	public Optional<Boolean> delete(String id) {
		try {
			this.formationRepository.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch(EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}
	
}
