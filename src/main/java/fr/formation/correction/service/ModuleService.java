package fr.formation.correction.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.correction.repository.ModuleRepository;
import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Module;

@Service
public class ModuleService {

    @Autowired
    private ModuleRepository moduleRepository;

    public void create(Module m) {
        this.moduleRepository.save(m);
    }

    public List<Module> findAll() {
        return this.moduleRepository.findAll();
    }

    public Optional<Module> getById(Integer id) {
        return this.moduleRepository.findById(id);
    }

    public void update(Module m) {
        this.moduleRepository.save(m);
    }

    public Optional<Boolean> delete(Module m) {
        try {
            this.moduleRepository.delete(m);
            return Optional.of(Boolean.TRUE);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public Optional<Boolean> delete(Integer id) {
        try {
            this.moduleRepository.deleteById(id);
            return Optional.of(Boolean.TRUE);
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    public List<Module> findByFiliereId(Integer id) {
        Filiere f = new Filiere();
        f.setId(id);
        return moduleRepository.findByFiliere(f);
    }

    public List<Module> getModuleWithoutFormateur() {
        return moduleRepository.findByFormateurIsNull();
    }

    public List<Module> getModulesByFormateur(String formateur) {
        return new ArrayList<>();
        // TODO : implement findByFormateur
//        return moduleRepository.findByFormateur(formateur);
    }

}
