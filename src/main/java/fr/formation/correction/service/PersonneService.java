package fr.formation.correction.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import fr.formation.correction.model.Filiere;
import fr.formation.correction.model.Personne;
import fr.formation.correction.model.TypePersonne;
import fr.formation.correction.repository.PersonneRepository;

@Service
public class PersonneService {

	@Autowired
	private PersonneRepository personneRepository;

	public void create(Personne p) {
		this.personneRepository.save(p);
	}

	public List<Personne> findAll() {
		return this.personneRepository.findAll();
	}

	public Optional<Personne> getById(Integer id) {
		return personneRepository.findById(id);
	}

	public void update(Personne p) {
		this.personneRepository.save(p);
	}

	public Optional<Boolean> delete(Personne p) {
		try {
			this.personneRepository.delete(p);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public Optional<Boolean> delete(Integer id) {
		try {
			this.personneRepository.deleteById(id);
			return Optional.of(Boolean.TRUE);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	public List<Personne> getFormateursOfFiliere(Filiere f) {
		return personneRepository.findFormateursOfFiliere(f);
	}

	/*
	 * /!\ Cette méthode ne lancera pas d'exception si la personne n'est pas un
	 * stagiaire Attention à gérer ce comportement avant d'appeler cette méthode
	 */
	public void bindStagiairesToFiliere(List<Personne> pList, Filiere f) {
		for (Personne p : pList) {
			personneRepository.bindPersonneToFiliere(p.getId(), f);
		}
	}

	/*
	 * /!\ Cette méthode ne lancera pas d'exception si la personne n'est pas un
	 * stagiaire Attention à gérer ce comportement avant d'appeler cette méthode
	 */
	public void bindStagiaireToFiliere(Personne p, Filiere f) {
		personneRepository.bindPersonneToFiliere(p.getId(), f);
	}

	public Boolean checkAllStagiaire(List<Personne> pList) {
		for (Personne p : pList) {
			if (!TypePersonne.STAGIAIRE.equals(personneRepository.getById(p.getId()).getType()))
				return false;
		}
		return true;
	}
	
	public Personne getWithLogin(String login){
		
		List<String> listLogin = personneRepository.listAllLogin();
		if( ! listLogin.contains(login) ) { 
			return null;
		}
		
		personneRepository.findByLogin(login);
		Personne p = personneRepository.findByLogin(login).get();
		return p;
	}
	
	public List<Personne> getWithFormationSouhaitee(String formationSouhaitee){
		return personneRepository.findByFormationSouhaitee(formationSouhaitee);
	}
	
	public List<Personne> getAllFormateurs(){
		return personneRepository.findAllFormateur();
	}

	public List<Personne> getAllNouveaux(){
		return personneRepository.findAllNouveaux();
	}
	
}
