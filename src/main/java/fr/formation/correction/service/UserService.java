package fr.formation.correction.service;

import fr.formation.correction.model.User;
import fr.formation.correction.model.UserRole;
import fr.formation.correction.repository.UserRepository;
import fr.formation.correction.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserRoleRepository userRoleRepository;

    // TODO : implement password encryption
//    @Autowired
//    PasswordEncoder passwordEncoder;

    public Optional<User> getByUsername(String name) {
        return userRepository.findByUsername(name);
    }

    // Meme méthode pour update et create pour l'instant
    public User create(User u) {
        // TODO : implement password encryption
//        u.setPassword(passwordEncoder.encode(u.getPassword()));

        userRepository.save(u);

        // Roles
        for (UserRole userRole : u.getRoles()) {
            userRole.setUser(u);

            create(userRole);
        }
        return u;
    }

    public UserRole create(UserRole userRole) {
        return userRoleRepository.save(userRole);
    }

	// TODO implement subscription
    public void inscription(User u) {
//        UserRole defaultRole = new UserRole(u, Role.ROLE_USER);
        // 3 lignes qui marchent mais c'est mieux de passer par create()
//		u.setPassword(passwordEncoder.encode(u.getPassword()));
//		ur.save(u);
//		urr.save(defaultRole);
//        List<UserRole> listRole = new ArrayList<UserRole>();// u.getRoles();
//        listRole.add(defaultRole);
//        u.setRole(listRole);

//		u.setRole(Arrays.asList(defaultRole));

        create(u);
    }

}
